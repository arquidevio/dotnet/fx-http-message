﻿using System.Collections.Immutable;
using System.Web;
using Arquidev.Fx.HttpMessage.Core;

namespace Arquidev.Fx.HttpMessage.Adapters.HttpClient;

public static class Extensions
{
    /// <summary>
    ///     Creates <see cref="HttpMessageData" /> from <see cref="HttpRequestMessage" />
    /// </summary>
    /// <param name="request"></param>
    /// <remarks>
    ///     HttpRequestMessage.Content.LoadIntoBufferAsync() may need to be called if
    ///     <see cref="HttpRequestMessage.Content" /> is read multiple times
    /// </remarks>
    /// <returns></returns>
    public static async Task<HttpMessageData> ToMessageDataAsync(this HttpRequestMessage request)
    {
        ImmutableDictionary<string, string[]> ParseQuery()
        {
            if (request.RequestUri?.Query is not { } query) return ImmutableDictionary<string, string[]>.Empty;
            var parsed = HttpUtility.ParseQueryString(query);
            return parsed.AllKeys.ToImmutableDictionary(k => k!, k => new[] { parsed[k]! });
        }

        return new HttpMessageData
        {
            Headers = request.Headers
                .Select(d => new KeyValuePair<string, string[]>(d.Key, d.Value.ToArray()))
                .OrderBy(d => d.Key)
                .ToImmutableDictionary(d => d.Key, d => d.Value.ToArray()),
            Payload = request.Content is { } content
                ? await content.ReadAsByteArrayAsync()
                : Array.Empty<byte>(),
            Request = new HttpRequestData
            {
                Uri = request.RequestUri,
                Method = request.Method,
                Query = ParseQuery()
            }
        };
    }

    /// <summary>
    ///     Creates <see cref="HttpMessageData" /> from <see cref="HttpResponseMessage" />
    /// </summary>
    /// <param name="response"></param>
    /// <remarks>
    ///     HttpResponseMessage.Content.LoadIntoBufferAsync() may need to be called if
    ///     <see cref="HttpResponseMessage.Content" /> is read multiple times
    /// </remarks>
    /// <returns></returns>
    public static async Task<HttpMessageData> ToMessageDataAsync(this HttpResponseMessage response)
    {
        return new HttpMessageData
        {
            Headers = response.Headers
                .Select(d => new KeyValuePair<string, string[]>(d.Key, d.Value.ToArray()))
                .OrderBy(d => d.Key)
                .ToImmutableDictionary(d => d.Key, d => d.Value.ToArray()),
            Payload = response.Content is { } content
                ? await content.ReadAsByteArrayAsync()
                : Array.Empty<byte>(),
            Response = new HttpResponseData
            {
                StatusCode = response.StatusCode
            }
        };
    }
}
