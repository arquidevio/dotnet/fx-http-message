﻿using System.Collections.Immutable;
using System.Net;

namespace Arquidev.Fx.HttpMessage.Core;

public class HttpRequestData
{
    public Uri? Uri { get; init; }
    public required HttpMethod Method { get; init; }
    public IDictionary<string, string[]> Query { get; init; } = ImmutableDictionary<string, string[]>.Empty;
}

public class HttpResponseData
{
    public HttpStatusCode? StatusCode { get; init; }
}

public class HttpMessageMetadata : Dictionary<string, string?>
{
    /// <summary>
    /// Indicates that two or more messages are part of a larger logical set
    /// </summary>
    public Guid? SetId
    {
        get => Guid.Parse(this[nameof(SetId)]!);
        set => this[nameof(SetId)] = value?.ToString();
    }
  
    /// <summary>
    /// Friendly name of the message originating party
    /// </summary>
    public string? OriginName
    {
        get => this[nameof(OriginName)];
        set => this[nameof(OriginName)] = value!;
    }
    
    /// <summary>
    /// Friendly name of the message destination party
    /// </summary>
    public string? DestinationName
    {
        get => this[nameof(DestinationName)];
        set => this[nameof(DestinationName)] = value!;
    }
    
    /// <summary>
    /// Specifies a logical type name of the message
    /// </summary>
    public string? MessageType
    {
        get => this[nameof(MessageType)];
        set => this[nameof(MessageType)] = value!;
    }
    
    public static readonly HttpMessageMetadata Empty = new();
}

public class HttpMessageData
{
    /// <summary>
    /// Set if the message represents a request
    /// </summary>
    public HttpRequestData? Request { get; init; }

    /// <summary>
    /// Set if the message represents a response
    /// </summary>
    public HttpResponseData? Response { get; init; }

    /// <summary>
    /// Message http headers
    /// </summary>
    public IDictionary<string, string[]> Headers { get; init; } = ImmutableDictionary<string, string[]>.Empty;

    /// <summary>
    /// Message payload as UTF8 bytes
    /// </summary>
    public byte[] Payload { get; init; } = Array.Empty<byte>();

    /// <summary>
    /// Enables passing non-strictly-HTTP related context data
    /// </summary>
    public HttpMessageMetadata Metadata { get; init; } = HttpMessageMetadata.Empty;
}
