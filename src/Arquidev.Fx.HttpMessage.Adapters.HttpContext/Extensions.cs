﻿using System.Collections.Immutable;
using System.Net;
using Arquidev.Fx.HttpMessage.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;

namespace Arquidev.Fx.HttpMessage.Adapters.HttpContext;

public static class Extensions
{
    /// <summary>
    ///     Creates <see cref="HttpMessageData" /> from <see cref="HttpRequest" />
    /// </summary>
    /// <param name="httpRequest">Source request</param>
    /// <remarks>
    ///     EnableBuffering() may need to be called on <see cref="HttpRequest" /> if the request will be read multiple
    ///     times
    /// </remarks>
    /// <returns></returns>
    public static async Task<HttpMessageData> ToMessageDataAsync(this HttpRequest httpRequest)
    {
        using var ms = new MemoryStream();
        await httpRequest.Body.CopyToAsync(ms);
        return new HttpMessageData
        {
            Payload = ms.ToArray(),
            Headers = httpRequest.Headers.OrderBy(d => d.Key).ToImmutableDictionary(d => d.Key, d => d.Value.ToArray()),
            Request = new HttpRequestData
            {
                Uri = new Uri(httpRequest.GetEncodedUrl()),
                Method = new HttpMethod(httpRequest.Method),
                Query = httpRequest.Query.ToImmutableDictionary(d => d.Key, d => d.Value.ToArray()),
            }
        };
    }

    /// <summary>
    ///     Creates <see cref="HttpMessageData" /> from <see cref="HttpResponse" />
    /// </summary>
    /// <param name="httpResponse">Source response</param>
    /// <returns></returns>
    public static async Task<HttpMessageData> ToMessageDataAsync(this HttpResponse httpResponse)
    {
        using var ms = new MemoryStream();
        await httpResponse.Body.CopyToAsync(ms);
        return new HttpMessageData
        {
            Payload = ms.ToArray(),
            Headers =
                httpResponse.Headers.OrderBy(d => d.Key).ToImmutableDictionary(d => d.Key, d => d.Value.ToArray()),
            Response = new HttpResponseData
            {
                StatusCode = (HttpStatusCode)httpResponse.StatusCode
            }
        };
    }
}
