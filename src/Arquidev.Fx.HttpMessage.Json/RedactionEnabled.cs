using System.Text.Json;
using System.Text.Json.Serialization;

namespace Arquidev.Ext.HttpMessage.Json;

public class RedactionEnabled : JsonConverter<string>
{
    public static readonly RedactionEnabled Instance = new();

    public override bool CanConvert(Type typeToConvert)
    {
        return false;
    }

    public override string Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        throw new NotImplementedException("");
    }

    public override void Write(Utf8JsonWriter writer, string value, JsonSerializerOptions options)
    {
        throw new NotImplementedException();
    }
}
