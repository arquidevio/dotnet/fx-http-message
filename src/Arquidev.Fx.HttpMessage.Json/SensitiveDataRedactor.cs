using System.Text.Json;
using System.Text.Json.Serialization;

namespace Arquidev.Ext.HttpMessage.Json;

public class SensitiveDataRedactor : JsonConverter<string>
{
    public override string? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        return reader.GetString();
    }

    public override void Write(Utf8JsonWriter writer, string value, JsonSerializerOptions options)
    {
        writer.WriteStringValue(options.Converters.Contains(RedactionEnabled.Instance) ? "***" : value);
    }
}
