﻿using System.Collections.Immutable;
using System.Text.Json;
using Arquidev.Fx.HttpMessage.Core;

namespace Arquidev.Ext.HttpMessage.Json;

public static class Extensions
{
    private static readonly JsonSerializerOptions DefaultOptions = new JsonSerializerOptions().WithObfuscation();

    private static JsonSerializerOptions WithObfuscation(this JsonSerializerOptions options)
    {
        options.Converters.Add(RedactionEnabled.Instance);
        return options;
    }

    public static T? PayloadOf<T>(this HttpMessageData message, JsonSerializerOptions? options = null)
    {
        return JsonSerializer.Deserialize<T>(message.Payload, options);
    }

    /// <summary>
    ///     Creates a new <see cref="HttpMessageData" /> with sensitive data redacted
    /// </summary>
    /// <param name="message">Message to redact</param>
    /// <param name="sensitiveHeaders">List of headers to redact</param>
    /// <param name="options">Serialization options</param>
    /// <remarks>
    ///     It assumes <see cref="HttpMessageData.Payload" /> can be deserialized to <see cref="T" />.
    ///     All the properties marked with <see cref="SensitiveDataAttribute" /> will get redacted
    /// </remarks>
    /// <typeparam name="T">Payload type</typeparam>
    /// <returns>Redacted message</returns>
    public static HttpMessageData WithSensitiveDataRedacted<T>(this HttpMessageData message,
        string[]? sensitiveHeaders = null, JsonSerializerOptions? options = null)
    {
        var redactSensitiveDataOptions = options != null ? options.WithObfuscation() : DefaultOptions;
        return new HttpMessageData
        {
            Headers = sensitiveHeaders != null
                ? message.Headers.ToDictionary(h => h.Key, h =>
                    sensitiveHeaders.Contains(h.Key, StringComparer.InvariantCultureIgnoreCase)
                        ? new []{"***"}
                        : h.Value).ToImmutableDictionary(d => d.Key, d => d.Value.ToArray())
                : message.Headers,
            Payload = JsonSerializer.SerializeToUtf8Bytes(message.PayloadOf<T>(), redactSensitiveDataOptions),
            Request = message.Request,
            Response = message.Response
        };
    }
}
