using System.Text.Json.Serialization;

namespace Arquidev.Ext.HttpMessage.Json;

[AttributeUsage(AttributeTargets.Property)]
public class SensitiveDataAttribute : JsonConverterAttribute
{
    public SensitiveDataAttribute() : base(typeof(SensitiveDataRedactor))
    {
    }
}
