﻿module Arquidev.Ext.HttpMessage.Tests.App

open Expecto

[<EntryPoint>]
let main argv =
    Tests.runTestsInAssembly defaultConfig argv
