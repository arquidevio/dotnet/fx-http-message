module Arquidev.Ext.HttpMessage.Tests.HttpContext

open System
open System.IO
open System.Collections.Immutable
open Arquidev.Fx.HttpMessage.Adapters.HttpContext
open Arquidev.Fx.HttpMessage.Core
open Expecto
open Microsoft.AspNetCore.Http

let writeToStream (bytes: byte[]) =
    task {
        let ms = new MemoryStream()
        do! ms.WriteAsync(bytes).AsTask()
        do! ms.FlushAsync()
        ms.Seek(0, SeekOrigin.Begin) |> ignore
        return ms
    }

let withRequestBody (bytes: byte[]) (httpContext: HttpContext) =
    task {
        let! stream = writeToStream bytes
        httpContext.Request.Body <- stream
        return httpContext
    }

let withResponseBody (bytes: byte array) (httpContext: HttpContext) =
    task {
        let! stream = writeToStream bytes
        httpContext.Response.Body <- stream
        return httpContext
    }

let withTarget (method: string) (uri: string) (httpContext: HttpContext) =
    let uri = Uri(uri)
    httpContext.Request.Host <- HostString(uri.Authority)
    httpContext.Request.Scheme <- uri.Scheme
    httpContext.Request.Method <- method
    httpContext

let withRequestHeaders (headers: (string * string array) list) (httpContext: HttpContext) =
    for key, values in headers do
        httpContext.Request.Headers.Add(key, values)

    httpContext

let withResponseHeaders (headers: (string * string []) list) (httpContext: HttpContext) =
    for key, values in headers do
        httpContext.Response.Headers.Add(key, values)

    httpContext

[<Tests>]
let tests =

    let jsonBytes = "{'test':'json'}"B

    let headers =
        [ "Authorization", [| "Bearer eY" |]
          "Multiple", [| "A"; "B" |] ]

    let headersDict = (dict headers).ToImmutableDictionary()

    testList
        "HttpContext -> "
        [ testList
              "payload"
              [

                testTask "from HttpRequest" {
                    let! (httpContext: HttpContext) =
                        DefaultHttpContext()
                        |> withTarget "POST" "http://localhost"
                        |> withRequestBody jsonBytes

                    let! (data: HttpMessageData) = httpContext.Request.ToMessageDataAsync()

                    "Payload should not be empty"
                    |> Expect.equal data.Payload.Length jsonBytes.Length

                    "Payload should match"
                    |> Expect.sequenceEqual (data.Payload) jsonBytes
                }

                testTask "from HttpResponse" {
                    let! (httpContext: HttpContext) = DefaultHttpContext() |> withResponseBody jsonBytes
                    let! (data: HttpMessageData) = httpContext.Response.ToMessageDataAsync()

                    "Payload should not be empty"
                    |> Expect.equal data.Payload.Length jsonBytes.Length

                    "Payload should match"
                    |> Expect.sequenceEqual (data.Payload) jsonBytes
                } ]
          testList
              "headers"
              [ testTask "form HttpRequest" {
                    let httpContext =
                        DefaultHttpContext()
                        |> withTarget "GET" "https://test.com"
                        |> withRequestHeaders headers

                    let! (data: HttpMessageData) = httpContext.Request.ToMessageDataAsync()

                    "Headers should match" |> Expect.containsAll data.Headers headersDict
                }

                testTask "form HttpResponse" {
                    let httpContext = DefaultHttpContext() |> withResponseHeaders headers
                    let! (data: HttpMessageData) = httpContext.Response.ToMessageDataAsync()

                    "Headers should match" |> Expect.containsAll data.Headers headersDict
                } ] ]
