module Arquidev.Ext.HttpMessage.Tests.HttpClient

open System.Net.Http
open Arquidev.Fx.HttpMessage.Adapters.HttpClient
open Arquidev.Fx.HttpMessage.Core
open Expecto


[<Tests>]
let tests =
    testList
        "map from HttpClient"
        [ testTask "should map Payload from HttpRequestMessage" {

              use httpRequest = new HttpRequestMessage()
              let arr = "{'test':'json'}"B
              let payloadLenght = arr.Length
              httpRequest.Content <- new ByteArrayContent(arr)
              let! (data: HttpMessageData) = httpRequest.ToMessageDataAsync()

              "Payload should not be empty" |> Expect.equal data.Payload.Length payloadLenght
              "Payload should match" |> Expect.sequenceEqual (data.Payload) arr
          }

          testTask "should map Payload from HttpResponseMessage" {

              use httpResponse = new HttpResponseMessage()
              let arr = "{'test':'json'}"B
              let payloadLenght = arr.Length
              httpResponse.Content <- new ByteArrayContent(arr)
              let! (data: HttpMessageData) = httpResponse.ToMessageDataAsync()
              "Payload should not be empty" |> Expect.equal data.Payload.Length payloadLenght
              "Payload should match" |> Expect.sequenceEqual (data.Payload) arr
          }
          ]
